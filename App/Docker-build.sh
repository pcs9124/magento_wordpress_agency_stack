#!/bin/bash

image_name=$1
if [ "${image_name}" = "" ];
then
    echo "$0: Not specified image name"
	echo "Usage : $0 <image name>"
	exit
fi

docker build -t ${image_name} .
