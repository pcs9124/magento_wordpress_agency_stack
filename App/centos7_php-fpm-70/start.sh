#!/bin/bash

groupadd -r -g $HOSTING_GRP_ID $HOSTING_USER && useradd -r -M -u $HOSTING_USER_ID -g $HOSTING_GRP_ID -d $HOSTING_USER_HOME $HOSTING_USER
groupadd -r -g $WEB_APP_GRP_ID $WEB_APP_USER && useradd -r -M -u $WEB_APP_USER_ID -g $WEB_APP_GRP_ID -s /bin/nologin $WEB_APP_USER
gpasswd -a $WEB_APP_USER $HOSTING_USER
gpasswd -a $HOSTING_USER $WEB_APP_USER

chown $WEB_APP_USER:$WEB_APP_USER -R /var/lib/php/session
## start php-fpm after launchind container
php-fpm --allow-to-run-as-root --fpm-config /etc/php-fpm.conf -c /etc/php.ini

/bin/bash

