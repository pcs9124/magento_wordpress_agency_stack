#!/bin/bash
banner Luroconnect

DATE=`which date`
TIMESTAMP=`$DATE +%d-%m-%y-%H%M`

PHP_VERSION=$1
user_data_file=./user_data.txt

. $user_data_file
## check whether the container is there with $HOSTING_USER ##
docker ps -a | grep $HOSTING_USER-app
if [ $? = 0 ]
then
   echo "container is aleady the with the name $HOSTING_USER-app-server"
   echo "destroy existing one if you want to create new one"
   exit
fi

## check condition for userdata file and php version
if [ ! -f "$user_data_file" -o "$1" = "" ];
then
    echo "$0: File '${user_data_file}' not found or not specified PHP_VERSION name"
        echo "Usage : $0 PHP_VERSION"
        exit
fi

## creates env variables file which can while running docker container
env_file=~/env-file-$TIMESTAMP
cat > $env_file <<EOF
HOSTING_USER=$HOSTING_USER
HOSTING_USER_ID=`id -u $HOSTING_USER`
HOSTING_GRP_ID=`id -g $HOSTING_USER`
WEB_APP_USER=nginx
WEB_APP_USER_ID=`id -u nginx`
WEB_APP_GRP_ID=`id -g nginx`
HOSTING_USER_HOME=`awk -F: -v v="$HOSTING_USER" '{if ($1==v) print $6}' /etc/passwd`
EOF

## creates volume file which can call to specify which which volumes to mount
## volumes must specify in user_data.tct file
vol_file=~/volumes-$TIMESTAMP
cat > $vol_file <<EOF
$CODE_PATH:$CODE_PATH
$MEDIA_PATH:$MEDIA_PATH
EOF

## creates a loop for volumes to indicate all volumes in docker run
vol=''
while read -r line
do
  vol="${vol} -v ${line}";
done < "$vol_file"

## launch a New container for $HOSTING_USER (php backend) ##
container_name=$HOSTING_USER-app-server
container_id=$(docker run -dit --name ${container_name} $vol --env-file=$env_file centos7_php-fpm-$PHP_VERSION)
container_ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' $container_id)

rm $vol_file; rm $env_file;

echo "New container has been launched: '$container_name'"
echo "IP for the container '$container_name': $container_ip"

## check backend and insert if it is not there ##
backend_check=$(grep ${HOSTING_USER} /etc/nginx/lb.inc)
if [ "$backend_check" = '' ]
then
   sudo cat >> /etc/nginx/lb.inc << EOF
upstream ${HOSTING_USER}_backend { server $container_ip:9000; }
EOF
else
   sudo sed -i "/$HOSTING_USER/,+1 d" /etc/nginx/lb.inc;
   sudo cat >> /etc/nginx/lb.inc << EOF
upstream ${HOSTING_USER}_backend { server $container_ip:9000; }
EOF
fi

