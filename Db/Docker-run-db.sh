#!/bin/bash

banner Luroconnect

DATE=`which date`
TIMESTAMP=`$DATE +%d-%m-%y-%H%M`

MYSQL_VERSION=$1
user_data_file=./user_data.txt

. $user_data_file
## check whether the container is there with $HOSTING_USER ##
docker ps -a | grep $MYSQL_USER-db
if [ $? = 0 ]
then
   echo "container is aleady the with the name $MYSQL_USER-db-server"
   echo "destroy existing one if you want to create new one"
   exit
fi

## check condition for userdata file
if [ ! -f "$user_data_file" -o "$1" = "" ];
then
    echo "$0: File '${user_data_file}' not found or not specified MYSQL_VERSION name"
    echo "Usage : $0 MYSQL_VERSION"
    exit
fi

## creates env variables file which can while running docker container
env_file=~/env-file-$TIMESTAMP
cat > $env_file <<EOF
MYSQL_DATABASE=$MYSQL_DATABASE
MYSQL_USER=$MYSQL_USER
MYSQL_PASSWORD=$MYSQL_PASSWORD
MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD
EOF

## creates volume file which can call to specify which which volumes to mount
## volumes must specify in user_data.tct file
vol_file=~/volumes-$TIMESTAMP
cat > $vol_file <<EOF
$MYSQL_HOME_PATH:/var/lib/mysql
$SQL_FILE_PATH:/docker-entrypoint-initdb.d
EOF

## creates a loop for volumes to indicate all volumes in docker run
vol=''
while read -r line
do
  vol="${vol} -v ${line}";
done < "$vol_file"

## launch a New container for $MYSQL_USER##
container_name=$MYSQL_DATABASE-db-server
container_id=$(docker run -d --name $MYSQL_DATABASE-db-server $vol --env-file=$env_file mysql:$MYSQL_VERSION)
container_ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' $container_id)

rm $vol_file; rm $env_file;

echo "New container has been launched: '$container_name'"
echo "IP for the container '$container_name': $container_ip"

echo -e "please change db access details in \n local.xml (or) \n env.php (or) \n wp-config.php as follows"
echo "MYSQL_DATABASE=$MYSQL_DATABASE"
echo "MYSQL_USER=$MYSQL_USER"
echo "MYSQL_PASSWORD=$MYSQL_PASSWORD"
echo "MYSQL_HOST=$container_ip"

