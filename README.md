# magento_wordpress_agency_stack

luroConnect Agency Stack
What type of development stack does a magento or wordpress agency need?

Many agencies complain of runaway cloud bills. Using docker vs instances might just be a better idea. Use these instructions and deploy your own docker images for your development work.

# Launch Docker container for app #

## Pre-requirements ##
1. staging user name
2. code path (where the code is placed)
3. media path (where the media is places)

Note: media path is not necessary if the media is a part code or media is not required

clone the code from magento_wordpress_agency_stack
```
#!python
git clone https://pcs9124@bitbucket.org/pcs9124/magento_wordpress_agency_stack.git
cd magento_wordpress_agency_stack/App
```

* check whether docker image has built as per required PHP version

```
#!python

docker images |grep centos7_php-fpm-<PHP-version>
```
* build if the image (skip this step if images is there)

```
#!python
cd centos7_php-fpm-<PHP-version>
../Docker-build.sh <PHP-version>
## where PHP-version as 72 or 70 or 56 and so on
```
## Launch docker container once build is done ##
* go to the location and edit "user_data.txt"

```
#!python
cd magento_wordpress_agency_stack/App

cat > user_data.txt << EOF
## specified user must already be created
HOSTING_USER=staging_user

## code path where the code is placed
## for ex. /home/staging1/www
CODE_PATH=/home/staging_user/www

## media path is where the media is placed
## not necessary if the media is there in code itself
MEDIA_PATH=
EOF
```
* Run Docker-run script
```
#!python

sh Docker-run-app-server.sh <PHP-version>
```

* check whether docker container launcher for app

```
#!python

docker ps -a |grep <staging_user>-app-server
```

## Login to the Container Logout from the Container##
* login to container
```
#!python

docker attach <staging_user>-app-server
```
* logout from container

```
#!python

ctrl+pq
## Note: we can user ctrl+d too, but it will stop the container
```
## Start, stop and destroy the container ##
* To Start

```
#!python

docker start <staging_user>-app-server
```
* To Stop
```
docker stop <staging_user>-app-server
```
* To Destroy
```
#!bash
docker rm <staging_user>-app-server
## Note: Container should be in stopped state to destroy 
```

# Launch Docker container for db #

## Pre-requirements ##
1. mysql_user
2. mysql_db_name
3. mysql_db_password
4. mysql_root_password
5. mysql_home_path (where the db has to store in host machine) (A new empty directory is preferable)
6. sql_file_path (where the .sql files are placed to update in mysql container)

clone the code from magento_wordpress_agency_stack

```
#!python
git clone https://pcs9124@bitbucket.org/pcs9124/magento_wordpress_agency_stack.git
cd magento_wordpress_agency_stack/Db
```
## Launch MySql docker container ##
* edit "user_data.txt"
cd magento_wordpress_agency_stack/Db
```
#!python
cat > user_data.txt << EOF
## Db_name
MYSQL_DATABASE=<staging_user>
## Db_user (mostly Db_name and Db_user both are same is preferred)
MYSQL_USER=<staging_user>
## Db_password 
MYSQL_PASSWORD=<16 digit random password>
## MySql Root password
MYSQL_ROOT_PASSWORD=<16 digit random password>
## where the db has to store in host machine
MYSQL_HOME_PATH=/data/docker_mysql/staging
## where the .sql files are placed to update in mysql container
SQL_FILE_PATH=/path/to/sql/dir
EOF
```
Note: Get Random password from [Here](https://passwordsgenerator.net/).

## Places sql files which need to update in mysql container ##
* Get live backup sql file and move to db_name.sql
```
#!bash
## copy live db sql file from backup server to <SQL_FILE_PATH>
mv <customer>.sql <SQL_FILE_PATH>/<db_user>_alive.sql
```
* Get core_config_data update sql file in the same location where main sql is placed
```
#!bash
mv /path/to/ccd.sql <SQL_FILE_PATH>/<db_user>_ccd.sql
```
Note: Both the sql files should be at one place and naming convention is important here.

## Launch the container ##
* Go to the path and run Docker-run-db.sh 
```
#!bash
cd magento_wordpress_agency_stack/Db

sh Docker-run-db.sh <MySql version say 5.6>
```
Note: DB update take some time depending upon db size and disk speed.
Try to login to db with the db access details from result to know whether db is updated or not.
## Get the DB access details from the result and change in  ##
* local.xml     (magento 1.x)
* env.php       (magento 2.x)
* wp-config.php (wordpress)
